import React from "react";
import MainPageContainer from "../MainPageContainer";

function App() {
  return <MainPageContainer />;
}

export default App;
